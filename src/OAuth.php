<?php

namespace SJRoyd\PEF;

use GuzzleHttp\Exception\ClientException;
use SJRoyd\PEF\Error\AuthError;
use SJRoyd\PEF\Helper\Service;
use SJRoyd\PEF\Response\AuthToken;

class OAuth extends Service
{
    protected $path = '/oauth2';

    /**
     *
     * @param string $id User ID
     * @param string $secret User secret
     */
    public function authorize($id, $secret)
    {
        try {
            $res = $this->call('POST', '/token', [
                'form_params' => [
                    'client_id'     => $id,
                    'client_secret' => $secret,
                    'grant_type'    => 'client_credentials',
                ]
            ]);
        } catch (ClientException $ex) {
            $res = $ex->getResponse();
        }
        $this->cast($res, [
            200   => AuthToken::class,
            '4..' => AuthError::class
        ]);

        self::$authToken = $this->responseData;
    }

}
