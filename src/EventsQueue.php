<?php

namespace SJRoyd\PEF;

use GuzzleHttp\Exception\BadResponseException;
use SJRoyd\PEF\Error\ClientError;
use SJRoyd\PEF\Error\ServerError;
use SJRoyd\PEF\Response\Message;

class EventsQueue extends Helper\Service
{
    use Helper\AuthRequire;

    protected $path = 'events-queue';

    /**
     * @var int
     */
    private $queueLength;

    /**
     * The first message in queue
     * @return Message
     */
    public function getMessage()
    {
        try {
            $res = $this->call('GET', '/messages');
            $this->queueLength = $res->getHeader('X-PEF-QueueLength');
        } catch (BadResponseException $ex) {
            $res = $ex->getResponse();
        }
        $this->cast($res, [
            200   => Message::class,
            204   => Message::class,
            '4..' => ClientError::class,
            '5..' => ServerError::class,
        ]);

        return $this->responseData;
    }

    /**
     * Queue length
     * @return int
     */
    public function getQueueLength()
    {
        return $this->queueLength;
    }

    /**
     * Acknowledge read of the message
     * @param string $messageId UUID
     * @return boolean
     */
    public function sendMessageAck($messageId)
    {
        try {
            $res = $this->call('DELETE', "/messages/{$messageId}");
        } catch (BadResponseException $ex) {
            $res = $ex->getResponse();
        }
        $this->cast($res, [
            '4..' => ClientError::class,
            '5..' => ServerError::class,
        ]);

        return true;
    }

}
