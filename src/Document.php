<?php

namespace SJRoyd\PEF;

use GuzzleHttp\Exception\BadResponseException;
use SJRoyd\PEF\Error\ClientError;
use SJRoyd\PEF\Error\ServerError;
use SJRoyd\PEF\Helper\DocumentFormat;

class Document extends Helper\Service
{
    use Helper\Document;
    use Helper\AuthRequire;

    protected $path = 'documents';

    /**
     * Send the document to the Broker
     * @param string|resource $document
     * @param string $docType   Helper\Document\Type
     * @param string $docFormat Helper\Document\Format
     * @see Helper\Document\Type
     * @see Helper\Document\Format
     * @return true
     */
    public function sendDocument($document, $docType, $docFormat = DocumentFormat::UBL)
    {
        $this->validDocumentType($docType);
        $this->validDocumentFormat($docFormat);

        $stream = \GuzzleHttp\Psr7\stream_for($document);
        try {
            $res = $this->call('POST', '', [
                'headers' => [
                    'X-PEF-DocumentType'   => $docType,
                    'X-PEF-DocumentFormat' => $docFormat,
                ],
                'body' => $stream
            ]);
        } catch (BadResponseException $ex) {
            $res = $ex->getResponse();
        }
        $this->cast($res, [
            '4..' => ClientError::class,
            '5..' => ServerError::class,
        ]);

        return true;
    }

    /**
     * Get the document content
     * @param string $documentId UUID
     * @param array $options Guzzle client options
     * @return string
     */
    public function getDocumentContent($documentId, $options = [])
    {
        try {
            $res = $this->call('GET', "/{$documentId}/content", $options);
        } catch (BadResponseException $ex) {
            $res = $ex->getResponse();
        }
        $this->cast($res, [
            '4..' => ClientError::class,
            '5..' => ServerError::class,
        ]);

        return $this->responseData;
    }

    /**
     * Acknowledge read of the document
     * @param string $documentId UUID
     * @return true
     */
    public function readDocument($documentId)
    {
        try {
            $res = $this->call('POST', "/{$documentId}/read-confirmation");
        } catch (BadResponseException $ex) {
            $res = $ex->getResponse();
        }
        $this->cast($res, [
            '4..' => ClientError::class,
            '5..' => ServerError::class,
        ]);

        return true;
    }



}
