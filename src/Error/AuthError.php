<?php

namespace SJRoyd\PEF\Error;

class AuthError extends \Exception
{

    public function setErrorDescription($message)
    {
        $this->message = $message;
    }

    public function setStatus($code)
    {
        $this->code = $code;
    }
}
