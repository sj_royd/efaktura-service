<?php

namespace SJRoyd\PEF\Error;

class ClientError extends \Exception
{

    public function setDescription($text)
    {
        $this->message = $text;
    }

    public function setStatus($status)
    {
        $this->code = $status;
    }
}
