<?php

namespace SJRoyd\PEF\Error;

class ServerError extends \Exception
{
    /**
     * @var string
     */
    public $errorId;

    /**
     * @var \DateTime
     */
    public $timestamp;

    public function __construct($message = "", $code = 500, \Throwable $previous = NULL)
    {
        parent::__construct($message, $code, $previous);
    }

    public function setDescription($text)
    {
        $this->message = $text;
    }
}
