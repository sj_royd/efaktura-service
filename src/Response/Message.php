<?php

namespace SJRoyd\PEF\Response;

class Message
{
    /**
     * @var Message\ReceivedDocumentStatusChanged
     */
    public $receivedDocumentStatusChangedMessage;

    /**
     * @var Message\SentDocumentStatusChanged
     */
    public $sentDocumentStatusChangedMessage;

    /**
     * @var Message\SentDocumentErrorMessage
     */
    public $sentDocumentErrorMessage;

    /**
     * @var Message\DocumentReceived
     */
    public $documentReceivedMessage;

    /**
     * @var Message\DocumentSentFromOtherSource
     */
    public $documentSentFromOtherSourceMessage;

    /**
     * @var bool
     */
    public $getDocumentContent = false;

    /**
     * @param Message\DocumentReceived $msg
     */
    public function setDocumentReceivedMessage(Message\DocumentReceived $msg) {
        $this->documentReceivedMessage = $msg;
        $this->getDocumentContent = true;
    }

    /**
     * @param Message\DocumentSentFromOtherSource $msg
     */
    public function setDocumentSentFromOtherSourceMessage(Message\DocumentSentFromOtherSource $msg) {
        $this->documentSentFromOtherSourceMessage = $msg;
        $this->getDocumentContent = true;
    }

    /**
     *
     */
    public function getMessage()
    {
        if($this->receivedDocumentStatusChangedMessage){
            return $this->receivedDocumentStatusChangedMessage;
        }
        if($this->sentDocumentStatusChangedMessage){
            return $this->sentDocumentStatusChangedMessage;
        }
        if($this->sentDocumentErrorMessage){
            return $this->sentDocumentErrorMessage;
        }
        if($this->documentReceivedMessage){
            return $this->documentReceivedMessage;
        }
        if($this->documentSentFromOtherSourceMessage){
            return $this->documentSentFromOtherSourceMessage;
        }
    }

}
