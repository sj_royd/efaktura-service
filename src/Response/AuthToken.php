<?php

namespace SJRoyd\PEF\Response;

class AuthToken
{
    /**
     * @var string
     */
    public $access_token;

    /**
     * @var string
     */
    public $token_type;

    /**
     * @var \DateTime
     */
    public $expires_in;

    /**
     * @var string
     */
    public $refresh_token;


    public function setExpiresIn($seconds)
    {
        $this->expires_in = new \DateTime();
        $newTs = $this->expires_in->getTimestamp() + $seconds;
        $this->expires_in->setTimestamp($newTs);
    }

    public function getHeaderData()
    {
        return "{$this->token_type} {$this->access_token}";
    }
}
