<?php

namespace SJRoyd\PEF\Response\Message;

use SJRoyd\PEF\Helper\Message;
use SJRoyd\PEF\Helper\Status\DocumentSend;

class SentDocumentStatusChanged
{
    use Message;

    /**
     *
     * @var string
     * @see DocumentSend
     */
    public $status;
}
