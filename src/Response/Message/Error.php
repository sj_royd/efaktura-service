<?php

namespace SJRoyd\PEF\Response\Message;

class Error
{
    /**
     * @var string
     */
    public $errorCode;

    /**
     * @var string
     */
    public $errorMessage;
}
