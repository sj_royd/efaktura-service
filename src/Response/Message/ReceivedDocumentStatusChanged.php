<?php

namespace SJRoyd\PEF\Response\Message;

use SJRoyd\PEF\Helper\Message;
use SJRoyd\PEF\Helper\Status\DocumentReceived;

class ReceivedDocumentStatusChanged
{
    use Message;

    /**
     *
     * @var string
     * @see DocumentReceived
     */
    public $status;
}
