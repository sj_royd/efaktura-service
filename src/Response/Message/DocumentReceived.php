<?php

namespace SJRoyd\PEF\Response\Message;

use SJRoyd\PEF\Helper\Message;
use SJRoyd\PEF\Helper\Document;

class DocumentReceived
{
    use Message;

    /**
     * @var BusinessValidationReport
     */
    public $businessValidationReport;

    /**
     * @var string
     * @see Document\Type
     */
    public $documentType;
}
