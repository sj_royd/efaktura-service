<?php

namespace SJRoyd\PEF\Response\Message;

use SJRoyd\PEF\Helper\Message;
use SJRoyd\PEF\Helper\Document;

class DocumentSentFromOtherSource
{
    use Message;

    /**
     * @var string
     * @see Document\Type
     */
    public $documentType;
}
