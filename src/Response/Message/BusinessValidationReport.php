<?php

namespace SJRoyd\PEF\Response\Message;

use SJRoyd\PEF\Helper\Message;

class BusinessValidationReport
{

    /**
     * @var \DateTime
     */
    public $reportDate;

    /**
     * @var array
     * @see Message\Warning
     */
    public $warnings;
}
