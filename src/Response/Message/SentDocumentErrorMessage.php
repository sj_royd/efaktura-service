<?php

namespace SJRoyd\PEF\Response\Message;

use SJRoyd\PEF\Helper\Message;

class SentDocumentErrorMessage
{
    use Message;

    /**
     *
     * @var Error[]
     */
    public $errors;
}
