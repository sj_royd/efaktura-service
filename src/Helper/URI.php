<?php

namespace SJRoyd\PEF\Helper;

class URI
{

    const PEF_EXPERT     = 'https://api.brokerpefexpert.efaktura.gov.pl/api/v1/';
    const PEF_EXPERT_INT = 'https://api.int-brokerpefexpert.efaktura.gov.pl/api/v1/';
    const INFINITE       = 'https://api-int.brokerinfinite.efaktura.gov.pl/api/v1/';
    const INFINITE_INT   = 'https://api-int-integrator.lab.brokerinfinite.efaktura.gov.pl/api/v1/';

    /**
     * Gets the PEFExpert broker API uri
     * @param boolean $integration
     * @return string
     */
    public static function getPEFExpert($integration = false)
    {
        return $integration ? self::PEF_EXPERT_INT : self::PEF_EXPERT;
    }

    /**
     * Gets the Infinite broker API uri
     * @param boolean $integration
     * @return string
     */
    public static function getInfinite($integration = false)
    {
        return $integration ? self::INFINITE_INT : self::INFINITE;
    }

}
