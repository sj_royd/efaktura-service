<?php

namespace SJRoyd\PEF\Helper;

trait Constants
{

    public static function getList()
    {
        $oClass = new \ReflectionClass(__CLASS__);
        return $oClass->getConstants();
    }

    public static function exists($const)
    {
        return in_array($const, self::getList());
    }

}
