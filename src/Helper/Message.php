<?php

namespace SJRoyd\PEF\Helper;

trait Message
{
    /**
     * @var string
     */
    public $messageId;

    /**
     * @var string
     */
    public $documentId;
}
