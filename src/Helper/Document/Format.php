<?php

namespace SJRoyd\PEF\Helper\Document;

use SJRoyd\PEF\Helper\Constants;

class Format
{
    use Constants;

    const UBL    = 'Ubl';
    const CEFACT = 'Cefact';

}
