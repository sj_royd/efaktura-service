<?php

namespace SJRoyd\PEF\Helper\Document;

use SJRoyd\PEF\Helper\Constants;

class Type
{
    use Constants;

    const CREDIT_NOTE        = 'CREDIT_NOTE';
    const DESPATCH_ADVICE    = 'DESPATCH_ADVICE';
    const INVOICE            = 'INVOICE';
    const INVOICE_CORRECTION = 'INVOICE_CORRECTION';
    const ORDER              = 'ORDER';
    const RECEIPT_ADVICE     = 'RECEIPT_ADVICE';

}
