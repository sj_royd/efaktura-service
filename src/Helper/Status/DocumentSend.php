<?php

namespace SJRoyd\PEF\Helper\Status;

use SJRoyd\PEF\Helper\Constants;

class DocumentSend
{
    use Constants;

    const PENDING   = 'PENDING';
    const SENT      = 'SENT';
    const RECEIVED  = 'RECEIVED';
    const READ      = 'READ';

}
