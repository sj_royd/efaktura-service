<?php

namespace SJRoyd\PEF\Helper\Status;

use SJRoyd\PEF\Helper\Constants;

class DocumentReceived
{
    use Constants;

    const RECEIVED  = 'RECEIVED';
    const READ      = 'READ';

}
