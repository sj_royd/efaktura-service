<?php

namespace SJRoyd\PEF\Helper;

trait Document
{

    /**
     *
     * @param string $type
     * @return boolean
     */
    protected function validDocumentType($type)
    {
        if(!Document\Type::exists($type)){
            throw new \InvalidArgumentException("Document type {$type} is invalid. "
            . "Valid types is ".implode(',', $this->types));
        }
    }

    protected function validDocumentFormat($format)
    {
        if(!Document\Format::exists($format)){
            throw new \InvalidArgumentException("Document format {$format} is invalid. "
            . "Valid formats is ".implode(',', $this->formats));
        }
    }

}
