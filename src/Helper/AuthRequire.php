<?php

namespace SJRoyd\PEF\Helper;

trait AuthRequire
{
    public function __construct()
    {
        parent::__construct();

        if(!self::$authToken){
            throw new \InvalidArgumentException('Client not authorized. Please authorize by (new OAuth)->authorize(id, secret)');
        }
    }
}
