<?php

namespace SJRoyd\PEF\Helper;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use SJRoyd\PEF\Response\AuthToken;

abstract class Service
{

    /**
     * Service URI
     * @var string
     */
    protected static $uri;

    /**
     * Service main path
     * @var string
     */
    protected $path;

    /**
     * Guzzle HTTP Client
     * @var Client
     */
    protected $client;

    /**
     * The response status code
     * @var int
     */
    protected $statusCode;

    /**
     * The response body
     * @var mixed
     */
    protected $responseData;

    /**
     * Authorisation token
     * @var AuthToken
     */
    protected static $authToken;

    public function __construct()
    {
        if(!self::$uri){
            throw new \InvalidArgumentException('Client URI is not defined. Please use Service::setBrokerInfinite() or Service::setBrokerPefExpert() before the instance');
        }
        $this->client = new Client([
            'base_uri' => self::$uri
        ]);
    }

    protected function call($method, $uri, $opts = [])
    {
        self::$authToken && $opts['headers']['Authorization'] = self::$authToken->getHeaderData();

        return $this->client->request($method, $this->path.$uri, $opts);
    }

    /**
     * Sets Infinite broker
     * @param boolean $integration
     */
    public static function setBrokerInfinite($integration = false)
    {
        self::$uri = URI::getInfinite($integration);
    }

    /**
     * Sets PEFExpert broker
     * @param boolean $integration
     */
    public static function setBrokerPefExpert($integration = false)
    {
        self::$uri = URI::getPEFExpert($integration);
    }

    /**
     *
     * @param ResponseInterface $resource
     * @param array $map
     */
    protected function cast($resource, $map = [])
    {
        $code = $resource->getStatusCode();

        $data = $resource->getBody();
        if($map && ($mapClass = self::getCastObj($map, $code))){
            $obj = json_decode($data->__toString() ?: '{}');
            $obj->status = $code;
            $data = (new \JsonMapper)->map($obj, new $mapClass);
        }

        if($data instanceof \Exception){
            throw $data;
        }

        $this->responseData = $data;

    }

    /**
     *
     * @param array $castMap
     * @param int $code
     * @return boolean|object
     */
    private static function getCastObj($castMap, $code)
    {
        foreach($castMap as $codePattern => $obj){
            $pattern = "~{$codePattern}~";
            preg_match($pattern, $code, $m);
            if($m){
                return $obj;
            }
        }
        return false;
    }

}
