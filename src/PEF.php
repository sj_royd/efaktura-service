<?php


namespace SJRoyd\PEF;

/**
 * @property OAuth $auth
 * @property EventsQueue $queue
 * @property Document $document
 */
class PEF
{
    const BROKER_PEFEXPERT = 'PEFExpert';
    const BROKER_INFINITE = 'Infinite';

    /**
     * @var OAuth
     */
    private $auth;

    /**
     * @var EventsQueue
     */
    private $queue;

    /**
     * @var Document
     */
    private $document;

    /**
     * @param string $id User ID
     * @param string $secret User secret
     * @param string $broker Name of broker
     * @param boolean $int If integration
     */
    public function __construct($id, $secret, $broker, $int = false)
    {
        switch($broker){
            case self::BROKER_PEFEXPERT:
                Helper\Service::setBrokerPefExpert($int); break;
            case self::BROKER_INFINITE:
                Helper\Service::setBrokerInfinite($int); break;
        }

        $this->auth = new OAuth;
        $this->auth->authorize($id, $secret);

        $this->queue = new EventsQueue;

        $this->document = new Document;
    }

    public function __get($name)
    {
        return $this->$name;
    }
}
