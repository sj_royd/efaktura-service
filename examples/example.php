<?php

include '../vendor/autoload.php';

use SJRoyd\PEF\Error;
use SJRoyd\PEF\PEF;

try {

    $pefID = 'user id';
    $pefSecret = 'user secret';
    $pefService = PEF::BROKER_INFINITE;

    $pef = new PEF($pefID, $pefSecret, $pefService);

    $queueLength = 0;
    do {
        // get first message in queue
        $msgGeneral = $pef->queue->getMessage();
        $msg = $msgGeneral->getMessage();
        // get length of queue
        $queueLength = $pef->queue->getQueueLength();

        if($msg){
            // if document has content
            if($msgGeneral->getDocumentContent){
                // get document content
                $content = $pef->document->getDocumentContent($msg->documentId);
                // set document status as read
                $pef->document->readDocument($msg->documentId);
                // do something with document
            }
            // set message status as read
            // at the end cause document->getDocumentContent()
            // may throws an Error\ClientError or Error\ServerError
            // and we can get the message again
            $pef->queue->sendMessageAck($msg->messageId);
        }
    } while($queueLength > 0);

} catch (Error\AuthError $ex) {
    echo 'Auth error';
} catch (Error\ClientError $ex) {
    echo 'Client error';
} catch (Error\ServerError $ex) {
    echo 'Server error';
}

