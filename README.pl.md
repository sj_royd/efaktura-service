# Usługa Platformy Elektronicznego Fakturowania

Biblioteka umożliwia pobranie dokumentów oraz ich wysłanie do Platformy Elektronicznego Fakturowania (https://efaktura.gov.pl/).

## Dla kogo jest PEF

Platforma Elektronicznego Fakturowania (PEF) służy do przekazywania faktur i innych ustrukturyzowanych dokumentów między wykonawcami zamówień publicznych a instytucjami zamawiającymi. Usługi są świadczone równolegle przez dwóch Brokerów PEF.

Implementacje przesyłanych dokumentów zostały przygotowane w oparciu o wytyczne PEPPOL BIS 3.0 i CEFACT.

## 1. Instalacja

`$ composer require sj_royd/efaktura_service`

## 2. Użycie biblioteki

```php
<?php

use SJRoyd\PEF\PEF;
use SJRoyd\PEF\Error;

try {

    $pefID = 'user id'; // identyfikator użytkownika API
    $pefSecret = 'user secret'; // klucz użytkownika API
    $pefService = PEF::BROKER_INFINITE; // dostępne PEF::BROKER_PEFEXPERT oraz PEF::BROKER_INFINITE

    $pef = new PEF($pefID, $pefSecret, $pefService);

    $queueLength = 0;
    do {
        // pobiera pierwszą wiadomość z kolejki
        $msgGeneral = $pef->queue->getMessage();
        $msg = $msgGeneral->getMessage();

        // pobiera liczbę wiadomości w kolejce
        $queueLength = $pef->queue->getQueueLength();

        // jeśli istnieje wiadomość
        if($msg){
            // jeśli widomość wskazuje na istnienie dokumentu
            if($msgGeneral->getDocumentContent){
                // pobiera treść dokumentu
                $content = $pef->document->getDocumentContent($msg->documentId);

                // ustawia status dokumentu na przeczytany
                $pef->document->readDocument($msg->documentId);

                // tu robimy coś z dokumentem
            }

            // ustawia status wiadomości na przeczytany
            // na końcu, gdyż document->getDocumentContent()
            // może wyrzucić an Error\ClientError lub Error\ServerError
            // i możemy następnym razem wrócić do wiadomości
            $pef->queue->sendMessageAck($msg->messageId);
        }

    } while($queueLength > 1);

} catch (Error\AuthError $ex) {
    echo 'Auth error';
} catch (Error\ClientError $ex) {
    echo 'Client error';
} catch (Error\ServerError $ex) {
    echo 'Server error';
}
```

## 3. Operacje

- `getMessage()` Kolejka komunikatów oczekujących; lokalizacja `$pef->queue`

    Dla systemu zewnętrznego (identyfikowanego jako konto systemu zewnętrznego)
    System PEF utrzymuje kolejkę adresowanych do niego komunikatów. Operacja pozwala
    pobrać pierwszy czekający w kolejce komunikat. Ponowne wywołanie tej operacji
    zwróci ten sam komunikat do czasu potwierdzenia przez System zewnętrzny, za
    pomocą operacji `sendMessageAck()`, że ten komunikat został poprawnie przetworzony.

- `sendMessageAck()` Potwierdzenie odebrania komunikatu; lokalizacja `$pef->queue`

    Operacja pozwala zatwierdzić poprawne przetworzenie ostatniego pobranego
    komunikatu. Po wywołaniu tej operacji na kolejce komunikatów będzie dostępny nowy
    komunikat (jeśli takowy istnieje). Wywołanie tej operacji oznacza również odebranie
    dokumentu (w kontekście komunikatu dotyczącego nowego dokumentu przychodzącego).

- `getQueueLength()` Liczba oczekujących w kolejce komunikatów; lokalizacja `$pef->queue`

    Operacja zwraca liczbę oczekujących w kolejce komunikatów po uprzednim wykonamiu
    operacji `getMessage()`.

- `sendDocument()` Przekazanie dokumentu do wysyłki; lokalizacja `$pef->document`

    Operacja umożliwia przekazanie dokumentu do Sytemu PEF.

    Można wysyłać jeden z sześciu typów dokumentu - helper `Document\Type`

    Dokument przekazywany jest w formacie Ubl lub Cefact - helper `Document\Format`

    Odbiorca dokumentu ustalany jest na podstawie treści dokumentu.
    System PEF wysyła dokument do odbiorcy w sposób asynchroniczny. Informacje o
    statusie wysyłania dokumentu można pobrać z kolejki komunikatów. Jeżeli System PEF
    poprawnie przyjmie dokument, wtedy operacja ta zwraca identyfikator
    dokumentu (`documentId`). Identyfikator ten powinien być zapisany przez system
    zewnętrzny w celu powiązania komunikatów na temat tego dokumentu, które w przyszłości
    pojawią się na kolejce komunikatów.

- `getDocumentContent()` Pobranie treści dokumentu; lokalizacja `$pef->document`

    Operacja pozwala pobrać treść (w formacie Ubl) dokumentu odbieranego lub wysyłanego
    z innego źródła. Treść dokumentu jest dostępna przez 7 dni od czasu odebrania
    komunikatu o nowym dokumencie.

- `readDocument()` Zaznaczenie dokumentu jako odczytany; lokalizacja `$pef->document`

    Operacja pozwala przekazać informację, że użytkownik systemu zewnętrznego odczytał dokument.


## 4. Kolejka komunikatów

Operacja `getMessage()` zwraca obiekt SJRoyd\PEF\Response\Message, który zawiera
jedną z pięciu wiadomości (poniżej) oraz informację `getDocumentContent` mówiącą czy do wiadomości
przypisany jest dokument do pobrania metodą `getDocumentContent()`.

Metoda `getMessage()` na operacji `getMessage()` zwraca konkretną wiadomość,
która przyszła. Każda wiadomość posiada pole `messageId` oznaczające identyfikator
wiadomości oraz `documentId` będące identyfikatorem dokumentu.

Wiadomości można obsłużyć za pomocą funkcji `switch`:

```php
<?php

$msgGen = $pef->message->getMessage();
$msg = $msgGen->getMessage();
switch(true) {
    case $msg instanceof Message\ReceivedDocumentStatusChanged:
        // do something
        break;
    case $msg instanceof Message\SentDocumentStatusChanged:
        // do something
        break;
    // etc...
}
```

lub przy pomocy `if`:

```php
<?php

$msgGen = $pef->message->getMessage();
if(($msg = $msgGen->receivedDocumentStatusChangedMessage)){
    // do something
}
if(($msg = $msgGen->sentDocumentStatusChangedMessage)){
    // do something
}
// etc...
```

### 4.1 Wiadomość Message\ReceivedDocumentStatusChanged

Komunikat informujący o zmianie statusu odebranego dokumentu. Komunikat zostanie
wygerowany jeśli status dokumentu został zminiony za pomocą innego kanału
(aplikacji WEB lub aplikacji desktop).

Pola wiadomości:

- `messageId`: uuid

- `documentId`: uuid

- `status`: enum [ RECEIVED, READ ] - helper `Status\DocumentReceived`

### 4.2 Wiadomość Message\SentDocumentStatusChanged

Komunikat informujący o zmianie statusu wysłanego dokumentu.

Pola wiadomości:

- `messageId`: uuid

- `documentId`: uuid

- `status`: enum [ PENDING, SENT, RECEIVED, READ ] - helper `Status\DocumentSend`

### 4.3 Wiadomość Message\SentDocumentErrorMessage

Komunikat informujący o błędzie w procesie wysyłania dokumentu

Pola wiadomości:

- `messageId`: uuid

- `documentId`: uuid

- `errors`: list of Message\Error

    - `errorCode`: enum [ 401, 402, 403, 404, 501 ]

    - `errorMessage`: string


`errorCode` według słownika:

- 401 błędne żądanie

- 402 błąd walidacji treści dokumentu

- 403 endpoint odbiorcy dokumentu nie jest zarejestrowany w sieci PEPPOL

- 404 odbiorca nie obsługuje danego typu dokumentu

- 501 błąd dostarczenia dokumentu. Mimo prób ponowienia transmisji dostarczenie dokumentu nie było możliwe.

`errorMessage` - techniczny opis błędu. Opis ma na celu wskazać osobie technicznej
(nie użytkownikowi końcowemu) na czym polegał błąd. Opis może być w języku angielskim.


### 4.4 Wiadomość Message\DocumentReceived

Komunikat informujący o otrzymaniu dokumentu. Treść dokumentu można pobrać za
pomocą operacji `getDocumentContent()`.

Pola wiadomości:

- `messageId`: uuid

- `documentId`: uuid

- `businessValidationReport`: Message\BusinessValidationReport

    - `reportDate`: DateTime object

    - `warnings`: enum [ EMPTY_CONTRACT_ID, EMPTY_ORDER_REFERENCE, EMPTY_DESPATCH_DOCUMENT_REFERENCE, EMPTY_RECEIPT_DOCUMENT_REFERENCE, EMPTY_INVOICE_DOCUMENT_REFERENCE, REFERENCED_ORDER_NOT_FOUND, REFERENCED_DESPATCH_NOT_FOUND, REFERENCED_RECEIPT_NOT_FOUND, REFERENCED_INVOICE_NOT_FOUND ] - helper `Message\Warning`

- `documentType`: enum [ CREDIT_NOTE, DESPATCH_ADVICE, INVOICE, INVOICE_CORRECTION, ORDER, RECEIPT_ADVICE ] - helper `Document\Type`

### 4.5 Wiadomość Message\DocumentSentFromOtherSource

Komunikat informujący, że ze skrzynki obsługiwanej przez ten system, wysłano
dokument innym kanałem (z aplikacji WEB lub aplikacji desktop). Treść dokumentu
można pobrać za pomocą operacji `getDocumentContent()`.

Pola wiadomości:

- `messageId`: uuid

- `documentId`: uuid

- `documentType`: enum [ CREDIT_NOTE, DESPATCH_ADVICE, INVOICE, INVOICE_CORRECTION, ORDER, RECEIPT_ADVICE ] - helper `Document\Type`

## 5. Helper

Helpery posiadają metody statyczne:

- `getList(): list` - zwraca listę stałych w klasie

- `exists($const): boolean` - zwraca informację czy podana wartość istnieje jako stała

### Helper Document\Type

```php
<?php

namespace SJRoyd\PEF\Helper\Document;

use SJRoyd\PEF\Helper\Constants;

class Type
{
    use Constants;

    const CREDIT_NOTE        = 'CREDIT_NOTE';
    const DESPATCH_ADVICE    = 'DESPATCH_ADVICE';
    const INVOICE            = 'INVOICE';
    const INVOICE_CORRECTION = 'INVOICE_CORRECTION';
    const ORDER              = 'ORDER';
    const RECEIPT_ADVICE     = 'RECEIPT_ADVICE';

}
```

### Helper Document\Format

```php
<?php

namespace SJRoyd\PEF\Helper\Document;

use SJRoyd\PEF\Helper\Constants;

class Format
{
    use Constants;

    const UBL    = 'Ubl';
    const CEFACT = 'Cefact';

}
```

### Helper Status\DocumentReceived

```php
<?php

namespace SJRoyd\PEF\Helper\Status;

use SJRoyd\PEF\Helper\Constants;

class DocumentReceived
{
    use Constants;

    const RECEIVED  = 'RECEIVED';
    const READ      = 'READ';

}
```

### Helper Status\DocumentSend

```php
<?php

namespace SJRoyd\PEF\Helper\Status;

use SJRoyd\PEF\Helper\Constants;

class DocumentSend
{
    use Constants;

    const PENDING   = 'PENDING';
    const SENT      = 'SENT';
    const RECEIVED  = 'RECEIVED';
    const READ      = 'READ';

}
```

### Helper Message\Warning

```php
<?php

namespace SJRoyd\PEF\Helper\Message;

use SJRoyd\PEF\Helper\Constants;

class Warning
{
    use Constants;

    const EMPTY_CONTRACT_ID                 = 'EMPTY_CONTRACT_ID';
    const EMPTY_ORDER_REFERENCE             = 'EMPTY_ORDER_REFERENCE';
    const EMPTY_DESPATCH_DOCUMENT_REFERENCE = 'EMPTY_DESPATCH_DOCUMENT_REFERENCE';
    const EMPTY_RECEIPT_DOCUMENT_REFERENCE  = 'EMPTY_RECEIPT_DOCUMENT_REFERENCE';
    const EMPTY_INVOICE_DOCUMENT_REFERENCE  = 'EMPTY_INVOICE_DOCUMENT_REFERENCE';
    const REFERENCED_ORDER_NOT_FOUND        = 'REFERENCED_ORDER_NOT_FOUND';
    const REFERENCED_DESPATCH_NOT_FOUND     = 'REFERENCED_DESPATCH_NOT_FOUND';
    const REFERENCED_RECEIPT_NOT_FOUND      = 'REFERENCED_RECEIPT_NOT_FOUND';
    const REFERENCED_INVOICE_NOT_FOUND      = 'REFERENCED_INVOICE_NOT_FOUND';
}
```